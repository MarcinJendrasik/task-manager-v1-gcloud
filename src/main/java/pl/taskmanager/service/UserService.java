package pl.taskmanager.service;

import org.springframework.stereotype.Service;
import pl.taskmanager.entity.*;

@Service
public class UserService {

    public boolean isUserHasRoleDeveloperAndIsOwnerSprint(User user, Sprint sprint) {
        for (UserRole userRole : sprint.getUser().getUserRoles()) {

            if (userRole.isRoleDeveloper() && isOwnerSprint(user, sprint)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOwnerSprint(User user, Sprint sprint) {
        return sprint.getUser().equals(user);
    }

    public boolean isUserHasRoleDeveloperAndIsOwnerTask(User user, Task task) {
        for (UserRole userRole : task.getCreatedBy().getUserRoles()) {

            if (userRole.isRoleDeveloper() && isOwnerTask(user, task)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOwnerTask(User user, Task task) {
        return task.getCreatedBy().equals(user);
    }

    public boolean isUserHasRoleDeveloperAndIsOwnerTaskComment(User user, TaskComment taskComment) {
        for (UserRole userRole : taskComment.getUser().getUserRoles()) {

            if (userRole.isRoleDeveloper() && isOwnerTaskComment(user, taskComment)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOwnerTaskComment(User user, TaskComment taskComment) {
        return taskComment.getUser().equals(user);
    }
}
