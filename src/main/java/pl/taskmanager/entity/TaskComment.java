package pl.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "task_comment")
public class TaskComment {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotEmpty
	@Column(name = "comment")
	private String comment;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "created_by", nullable = false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "task_id", nullable = false)
	private Task task;

	@Column(name = "created_at")
	private Date createdAt;

	public int getId() {
		return id;
	}

	public TaskComment()
	{}

	public TaskComment(int id, String comment)
	{
		this.id = id;
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
