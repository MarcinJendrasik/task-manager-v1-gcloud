package pl.taskmanager.entity;

public enum SprintStatus {

    WAITING,
    ACTIVE,
    TERMINATED
}
