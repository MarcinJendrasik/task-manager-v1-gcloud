package pl.taskmanager.entity;

public enum TaskPriority {

    LOWEST,
    LOW,
    MEDIUM,
    HIGH,
    HIGHEST
}
