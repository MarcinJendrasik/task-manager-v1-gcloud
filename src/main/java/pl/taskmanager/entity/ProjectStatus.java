package pl.taskmanager.entity;

public enum ProjectStatus {
	
	ACTIVE,
	WAITING,
	SUSPENDED;

	public static boolean contains(String value) {

		for (ProjectStatus status : ProjectStatus.values()) {
			if (status.name().equals(value)) {
				return true;
			}
		}

		return false;
	}
}
