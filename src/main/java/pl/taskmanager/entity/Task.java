package pl.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "task")
public class Task {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotEmpty
	@Column(name = "name")
	private String name;

	@NotEmpty
	@Column(name = "description")
	private String description;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private TaskStatus status;

	@NotNull(message = "may not be empty")
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "assignee")
	private User assignee;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "priority")
	private TaskPriority priority;

	@NotNull
	@Min(1)
	@Max(30)
	@Column(name = "estimate")
	private Integer estimate;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "created_by", nullable = false)
	private User createdBy;

	@NotNull(message = "may not be empty")
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "sprint_id", nullable = false)
	private Sprint sprint;

	@JsonIgnore
	@OneToMany(mappedBy = "task")
	@OrderBy("id ASC")
	private Set<TaskComment> taskComments;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false)
	private Project project;

	@Column(name = "parent_id")
	private int parentId;

	@Column(name = "created_at")
	private Date createdAt;

	public Task()
	{}

	public Task(int id)
	{
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public User getAssignee() {
		return assignee;
	}

	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}

	public TaskPriority getPriority() {
		return priority;
	}

	public void setPriority(TaskPriority priority) {
		this.priority = priority;
	}

	public Integer getEstimate() {
		return estimate;
	}

	public void setEstimate(Integer estimate) {
		this.estimate = estimate;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	public Set<TaskComment> getTaskComments() {
		return taskComments;
	}

	public void setTaskComments(Set<TaskComment> taskComments) {
		this.taskComments = taskComments;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
