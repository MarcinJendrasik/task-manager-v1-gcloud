package pl.taskmanager.config;

import org.springframework.security.core.Authentication;

public class WebSecurity {

    public boolean isAnonymous(Authentication authentication)
    {
        if (authentication.getPrincipal() == "anonymousUser") {
            return true;
        }

        return false;
    }
}
