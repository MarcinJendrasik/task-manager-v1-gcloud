package pl.taskmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.taskmanager.config.WebSecurity;
import pl.taskmanager.entity.AuthorizedUser;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.UserRepository;

@Component
public class WebSecurityAccessUser extends WebSecurity{

    @Autowired
    UserRepository userRepository;

    public boolean check(Authentication authentication, int user_id)
    {
        if(isAnonymous(authentication))
            return false;

        AuthorizedUser customUserDetails = (AuthorizedUser)authentication.getPrincipal();

        User userParticipant = userRepository.findOneByUsername(customUserDetails.getUsername());
        User user = userRepository.findOne(user_id);

        if (userParticipant.equals(user) == false)
            return true;

        return false;
    }
}
