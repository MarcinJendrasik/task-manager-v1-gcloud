package pl.taskmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.taskmanager.config.WebSecurity;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.SprintRepository;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.repository.UserRoleRepository;
import pl.taskmanager.service.UserService;

@Component
public class WebSecurityAccessSprint extends WebSecurity {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    UserService userService;

    public boolean check(Authentication authentication, int projectId, int sprintId)
    {
        if(isAnonymous(authentication))
            return false;

        AuthorizedUser customUserDetails = (AuthorizedUser)authentication.getPrincipal();

        User user = userRepository.findOneByUsername(customUserDetails.getUsername());

        if (user.hasRoleAdmin())
            return true;

        Sprint sprint = sprintRepository.findOne(sprintId);

        if (userService.isUserHasRoleDeveloperAndIsOwnerSprint(user, sprint))
            return true;

        return false;
    }
}
