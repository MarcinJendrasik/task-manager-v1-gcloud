-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: task_manager
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci,
  `status` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project_user1_idx` (`created_by`),
  CONSTRAINT `FKepfrx92jw3n2lkjn2qqj1n93k` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_project_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'Task Manager','zadania','ACTIVE','2018-03-06 13:09:25',1);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_participant`
--

DROP TABLE IF EXISTS `project_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_participant` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  KEY `fk_project_participant_user1_idx` (`user_id`),
  KEY `fk_project_participant_project1_idx` (`project_id`),
  CONSTRAINT `FKbulk9pbt93ydex9307yj5aph8` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKd5nl58t7x785k2xerobepw6yn` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_project_participant_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_participant_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_participant`
--

LOCK TABLES `project_participant` WRITE;
/*!40000 ALTER TABLE `project_participant` DISABLE KEYS */;
INSERT INTO `project_participant` VALUES (1,1),(3,1),(4,1),(5,1),(6,1),(7,1);
/*!40000 ALTER TABLE `project_participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sprint`
--

DROP TABLE IF EXISTS `sprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sprint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE utf8_polish_ci NOT NULL,
  `goal` text COLLATE utf8_polish_ci,
  `project_id` int(11) NOT NULL,
  `status` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_stop` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2018-03-09 01:37:37',
  PRIMARY KEY (`id`),
  KEY `fk_sprint_project1_idx` (`project_id`),
  KEY `fk_sprint_user1_idx` (`created_by`),
  CONSTRAINT `FKerwve0blrvfhqm1coxo69f0xr` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `FKmoqx2noahp4g3dpkjtep0b37y` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_sprint_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sprint_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sprint`
--

LOCK TABLES `sprint` WRITE;
/*!40000 ALTER TABLE `sprint` DISABLE KEYS */;
INSERT INTO `sprint` VALUES (1,'Wstępny projekt v. 01','Ostatnie szlify',1,'ACTIVE',1,NULL,NULL,'2018-03-09 01:37:37');
/*!40000 ALTER TABLE `sprint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(180) NOT NULL,
  `description` text,
  `status` varchar(45) NOT NULL,
  `priority` varchar(45) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sprint_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `estimate` float NOT NULL,
  `project_id` int(11) NOT NULL,
  `assignee` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_task_sprint1_idx` (`sprint_id`),
  KEY `fk_task_user1_idx` (`created_by`),
  KEY `fk_task_project1_idx` (`project_id`),
  KEY `fk_task_user2_idx` (`assignee`),
  CONSTRAINT `FKagbc5lv4xged97pe511ahj35a` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FKao8fqiovc2g9en1pq7jaengh6` FOREIGN KEY (`sprint_id`) REFERENCES `sprint` (`id`),
  CONSTRAINT `FKeosvxgtnuynoegjnx2eo9eqpy` FOREIGN KEY (`assignee`) REFERENCES `user` (`id`),
  CONSTRAINT `FKk8qrwowg31kx7hp93sru1pdqa` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_task_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_task_sprint1` FOREIGN KEY (`sprint_id`) REFERENCES `sprint` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_task_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_task_user2` FOREIGN KEY (`assignee`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (1,'Bread crumbs (nawigacja)','Dodanie bread crumbs do wszystkich ekranów','DONE','LOWEST',0,1,1,1,1,3,'2018-03-09 01:37:37'),(3,'Usuwanie komentarzy(Owner dostepy)','Komentarze może usuwać Admin lub Developer który jest właścicielem komentarza','DONE','HIGH',0,1,1,1,1,1,'2018-03-09 01:37:37'),(5,'Komunikaty','Dodanie komunikatów do wszystkich ekranów. Komunikaty: błędów, sukcesów.','DONE','MEDIUM',0,1,1,1,1,5,'2018-03-09 01:37:37'),(8,'Wrzucenie projektu na Bitbucket','Tak jak w tytule.','TO_DO','HIGH',0,1,1,1,1,1,'2018-03-09 01:37:37'),(9,'Deploy (na produkcje)','Tak jak w tytule.','TO_DO','HIGH',0,1,1,1,1,1,'2018-03-10 14:05:37'),(11,'Stworzenie ekranu logowania','Tak jak w tytule.','DONE','MEDIUM',0,1,1,1,1,1,'2018-03-09 01:37:37'),(15,'Wyświetlanie polskich znaków','Błędy się pojawiają przy wyświetlaniu polskich znaków. Zmienic kodowanie w \"application.properties\"','DONE','LOW',0,1,1,1,1,1,'2018-03-09 01:37:37'),(16,'Input-y (modal)','Poprawić wielkości inputów (\".col-md-7\").','DONE','LOW',0,1,1,1,1,5,'2018-03-09 01:37:37'),(17,'Sortowanie taskow po ID (Backlog)','Tak jak w tytule.','DONE','LOW',0,1,1,1,1,1,'2018-03-09 01:37:37'),(18,'Do modala dodania linku do komentarza','Do modala dodania linku do komentarza','DONE','LOW',0,1,1,1,1,1,'2018-03-09 01:37:37'),(20,'Wypośrodkowanie tabeli (Backlog)','Tak jak w tytule','DONE','LOWEST',0,1,1,1,1,5,'2018-03-09 01:37:37'),(21,'Stopka(created by)','Tak jak w tytule.','DONE','LOWEST',0,1,1,1,1,1,'2018-03-09 01:37:37'),(27,'Created By (backlog)','Wyświetlenie imienia usera który stworzył task','DONE','MEDIUM',0,1,1,1,1,3,'2018-03-09 01:37:37'),(30,'Licznik komentarzy (Sprint)','Zrobienie licznika komentarzy ekran sprint','DONE','MEDIUM',0,1,1,1,1,1,'2018-03-09 01:37:37'),(33,'Zmiana statusu usera','Gdy user ma status enable na false to nie może sie zalogowac','DONE','MEDIUM',0,1,1,1,1,4,'2018-03-09 10:59:52'),(36,'Zrobić dump bazki','Tak jak w tytule','TO_DO','MEDIUM',0,1,1,1,1,1,'2018-03-09 11:10:58'),(45,'Wyswietlenie Informacji dotyczącej komentarza','Tak jak w tytule.','IN_PROGRESS','MEDIUM',0,1,1,1,1,1,'2018-03-12 14:39:23');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_comment`
--

DROP TABLE IF EXISTS `task_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `task_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_task_comment_task1_idx` (`task_id`),
  KEY `FKpcdi5jnpbufmsjfdyrxgwchry` (`created_by`),
  CONSTRAINT `FKpcdi5jnpbufmsjfdyrxgwchry` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FKpoxt1sd4otmq9p94rp1atkgs7` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `fk_task_comment_task1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_comment`
--

LOCK TABLES `task_comment` WRITE;
/*!40000 ALTER TABLE `task_comment` DISABLE KEYS */;
INSERT INTO `task_comment` VALUES (11,'Jakie informacje mają być wyświetlone?',45,3,'2018-03-12 14:38:33'),(12,'Comment by, date created',45,1,'2018-03-12 14:39:23');
/*!40000 ALTER TABLE `task_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_polish_ci NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'marcin','marcin.jendrasik@gmail.com',1,'$2a$10$vTyxjQp3AZDcFbGaJ.Bmy.A15SBUns3Q8SBT/psi.kno.LMDiAVj.'),(3,'daniel','daniel@wp.pll',1,'$2a$10$0qzBBvLulFt3GHkJUcBWquyFnXSxogJp6E96vo4NAoZBUCghM.TaK'),(4,'adam','adam@wp.pl',0,'$2a$10$6v0fP8bsCMHMr9n7dny.HuWv1qg2dNmBWoKDfrYNnSJFm06jX/6Qq'),(5,'artur','artur@wp.pl',1,'$2a$10$FNSDF7zadO.NkDlxS9rmi.Mq6hmTOo8kxOMRsyYPwZybd.7eyyw.y'),(6,'admin','admin@admin.pl',1,'$2a$10$yR2fNQI8nQEmjxhP3jM0auPqDprnC1pxSCCm7/EJ5ILT1ofr087La'),(7,'developer','developer@developer.pl',1,'$2a$10$EZ7vUTlCbH0F3JHHm.S7t.Zz5hsMz1aSz9skMQOmaOCa.g0DVcHOG');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_role`
--

DROP TABLE IF EXISTS `user_has_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_has_role` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `fk_user_has_role_user_role_idx` (`user_role_id`),
  KEY `fk_user_has_role_user1_idx` (`user_id`),
  CONSTRAINT `FKdtkvc2iy3ph1rkvd67yl2t13m` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKh307ey9wxnjth0ih8it2rsr0` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`),
  CONSTRAINT `fk_user_has_role_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_role_user_role` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_role`
--

LOCK TABLES `user_has_role` WRITE;
/*!40000 ALTER TABLE `user_has_role` DISABLE KEYS */;
INSERT INTO `user_has_role` VALUES (2,4),(2,3),(1,1),(1,6),(2,7),(1,5);
/*!40000 ALTER TABLE `user_has_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_DEVELOPER');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-20 20:25:38
