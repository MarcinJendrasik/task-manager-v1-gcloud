package pl.taskmanager.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import pl.taskmanager.entity.Task;
import pl.taskmanager.entity.TaskComment;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.TaskCommentRepository;
import pl.taskmanager.repository.TaskRepository;
import pl.taskmanager.repository.UserRepository;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.MockitoAnnotations.initMocks;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TaskCommentController.class)
@WithMockUser(username = "test")
public class TaskCommentControllerTest {

    @MockBean
    UserRepository userRepository;

    @MockBean
    TaskCommentRepository taskCommentRepository;

    @MockBean
    TaskRepository taskRepository;

    @MockBean
    BindingResult bindingResult;

    @Autowired
    MockMvc mvc;

    private static final int taskId = 1;
    private static final int taskCommentId = 2;

    @Before
    public void setUp()
    {
        initMocks(this);
    }

    @Test
    public void shouldReturnAllTaskCommentsByTaskId() throws Exception
    {
        Set<TaskComment> taskComments = new HashSet<TaskComment>();
        taskComments.add(new TaskComment(taskCommentId, "Test comment"));

        when(taskCommentRepository.findAllByTaskId(taskId)).thenReturn(taskComments);

        mvc.perform(
                get("/task/{task_id}/comment/findAll", taskId)
            )
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id", is(taskCommentId)))
            .andExpect(jsonPath("$[0].comment", is("Test comment")));

        verify(taskCommentRepository, times(1)).findAllByTaskId(taskId);
        verifyNoMoreInteractions(taskCommentRepository);
    }

    @Test
    public void shouldCreateTaskComment() throws Exception
    {
        User user = new User();
        user.setUsername("test");

        TaskComment taskComment = new TaskComment();
        taskComment.setComment("Test comment");
        taskComment.setCreatedAt(new Date());

        when(userRepository.findOneByUsername(user.getUsername())).thenReturn(user);
        when(taskRepository.findOne(taskId)).thenReturn(new Task());
        when(taskCommentRepository.save(taskComment)).thenReturn(taskComment);
        doNothing().when(taskRepository).flush();

        mvc.perform(
                post("/task/{task_id}/comment/add", taskId)
                .flashAttr("taskComment", taskComment)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            )
            .andExpect(status().isCreated());

        verify(userRepository, times(1)).findOneByUsername(user.getUsername());
        verify(taskRepository, times(1)).findOne(taskId);
        verify(taskCommentRepository, times(1)).save(taskComment);
        verify(taskRepository, times(1)).flush();
        verifyNoMoreInteractions(userRepository, taskRepository);
    }

    @Test
    public void cannotCreateTaskCommentWhenFieldIsEmptyThrowException() throws Exception
    {
        TaskComment taskComment = new TaskComment();

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                post("/task/{task_id}/comment/add", taskId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("taskComment", taskComment)
            )
            .andExpect(status().is(422))
            .andExpect(jsonPath("$.code", is("InvalidRequest")))
            .andExpect(jsonPath("$.message", is("Invalid task comment")))
            .andExpect(jsonPath("$.fieldErrorResources[0].resource", is("taskComment")))
            .andExpect(jsonPath("$.fieldErrorResources[0].field", is("comment")))
            .andExpect(jsonPath("$.fieldErrorResources[0].message", is("may not be empty")));
    }

    @Test
    public void shouldUpdateTaskComment() throws Exception
    {
        TaskComment taskComment = new TaskComment(taskCommentId, "Test comment");

        when(taskCommentRepository.findOne(taskCommentId)).thenReturn(taskComment);
        doNothing().when(taskCommentRepository).flush();

        mvc.perform(
                put("/task/{task_id}/comment/update/{task_comment_id}", taskId, taskCommentId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("taskComment", taskComment)
            )
            .andExpect(status().isOk());

        verify(taskCommentRepository, times(1)).findOne(taskCommentId);
        verify(taskCommentRepository, times(1)).flush();
        verifyNoMoreInteractions(taskCommentRepository);
    }

    @Test
    public void cannotUpdateTaskCommentWhenFieldIsEmptyThrowException() throws Exception
    {
        TaskComment taskComment = new TaskComment();

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                put("/task/{task_id}/comment/update/{task_comment_id}", taskId, taskCommentId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("taskComment", taskComment)
        )
            .andExpect(status().is(422))
            .andExpect(jsonPath("$.code", is("InvalidRequest")))
            .andExpect(jsonPath("$.message", is("Invalid task comment")))
            .andExpect(jsonPath("$.fieldErrorResources[0].resource", is("taskComment")))
            .andExpect(jsonPath("$.fieldErrorResources[0].field", is("comment")))
            .andExpect(jsonPath("$.fieldErrorResources[0].message", is("may not be empty")));
    }

    @Test
    public void shouldDeleteTaskCommentById() throws Exception
    {
        doNothing().when(taskCommentRepository).delete(taskCommentId);
        doNothing().when(taskCommentRepository).flush();

        mvc.perform(
                delete("/task/{task_id}/comment/delete/{task_comment_id}", taskId, taskCommentId)
            )
            .andExpect(status().isOk());

        verify(taskCommentRepository).delete(taskCommentId);
        verify(taskCommentRepository).flush();
        verifyNoMoreInteractions(taskCommentRepository);
    }
}
