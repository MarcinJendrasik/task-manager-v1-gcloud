package pl.taskmanager.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.SprintRepository;
import pl.taskmanager.repository.UserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SprintController.class)
@WithMockUser(username = "test")
public class SprintControllerTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private SprintRepository sprintRepository;

    @MockBean
    private BindingResult bindingResult;

    @Autowired
    private MockMvc mvc;

    private static final int projectId = 1;
    private static final int sprintId = 2;

    @Before
    public void setUp()
    {
        initMocks(this);
    }

    @Test
    public void shouldPrepareDataToViewSprint() throws Exception
    {
        Sprint sprint = new Sprint();
        List<Sprint> sprints = new ArrayList<>();

        List<User> users = new ArrayList<>();

        when(sprintRepository.findOne(sprintId)).thenReturn(sprint);
        when(sprintRepository.findByProjectId(projectId)).thenReturn(sprints);
        when(userRepository.findAll()).thenReturn(users);

        mvc.perform(
                get("/project/{project_id}/sprint/{sprint_id}", projectId, sprintId)
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))
            .andExpect(model().attribute("sprintId", sprintId))
            .andExpect(model().attribute("view", "sprint/index"))

            .andExpect(model().attribute("task", hasProperty("id", is(0))))
            .andExpect(model().attribute("task", hasProperty("name", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("description", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("assignee", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("estimate", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("priority", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("sprint", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("status", isEmptyOrNullString())))

            .andExpect(model().attribute("taskComment", hasProperty("id", is(0))))
            .andExpect(model().attribute("taskComment", hasProperty("comment", isEmptyOrNullString())))

            .andExpect(model().attribute("projectId", projectId))
            .andExpect(model().attribute("urlUpdateTaskStatus","/project/" + projectId + "/task/update/"))
            .andExpect(model().attribute("urlSaveTask","/project/" + projectId + "/task/add"))
            .andExpect(model().attribute("urlUpdateTask","/project/" + projectId + "/task/update/"))
            .andExpect(model().attribute("urlDeleteTask", "/project/" + projectId + "/task/delete/"))
            .andExpect(model().attribute("sprint", sprint))
            .andExpect(model().attribute("sprints", sprints))
            .andExpect(model().attribute("tasks", sprint.getTasks()))
            .andExpect(model().attribute("taskPriority", TaskPriority.values()))
            .andExpect(model().attribute("taskStatuses", TaskStatus.values()))
            .andExpect(model().attribute("users", users));

        verify(sprintRepository, times(1)).findOne(sprintId);
        verify(sprintRepository, times(1)).findByProjectId(projectId);
        verify(userRepository, times(1)).findAll();
        verifyNoMoreInteractions(sprintRepository, userRepository);
    }

    @Test
    public void shouldReturnSprintById() throws Exception
    {
        Sprint sprint = new Sprint();
        sprint.setName("test name");
        sprint.setGoal("test goal");
        sprint.setStatus(SprintStatus.ACTIVE);

        when(sprintRepository.findOne(sprintId)).thenReturn(sprint);

        mvc.perform(
                get("/project/{project_id}/sprint/findOne/{sprint_id}", projectId, sprintId)
            )
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.name", is("test name")))
            .andExpect(jsonPath("$.goal", is("test goal")))
            .andExpect(jsonPath("$.status", is(SprintStatus.ACTIVE.name())));

        verify(sprintRepository, times(1)).findOne(sprintId);
        verifyNoMoreInteractions(sprintRepository);
    }

    @Test
    public void shouldCreateSprint() throws Exception
    {
        User user = new User();
        user.setUsername("test");

        Project project = new Project();

        Sprint sprint = new Sprint();
        sprint.setName("test name");
        sprint.setGoal("test goal");
        sprint.setCreatedAt(new Date());
        sprint.setStatus(SprintStatus.ACTIVE);

        when(userRepository.findOneByUsername(user.getUsername())).thenReturn(user);
        when(projectRepository.findOne(projectId)).thenReturn(project);
        when(sprintRepository.save(sprint)).thenReturn(sprint);
        doNothing().when(projectRepository).flush();

        mvc.perform(
               post("/project/{project_id}/sprint/add", projectId)
               .contentType(MediaType.APPLICATION_FORM_URLENCODED)
               .flashAttr("sprint", sprint)
           )
           .andExpect(status().isCreated());

        verify(userRepository, times(1)).findOneByUsername(user.getUsername());
        verify(projectRepository, times(1)).findOne(projectId);
        verify(sprintRepository, times(1)).save(sprint);
        verify(projectRepository, times(1)).flush();
        verifyNoMoreInteractions(userRepository, projectRepository);
    }

    @Test
    public void shouldUpdateSprint() throws Exception
    {
        Sprint sprint = new Sprint();
        sprint.setName("test name");
        sprint.setGoal("test goal");
        sprint.setStatus(SprintStatus.ACTIVE);

        when(sprintRepository.findOne(sprintId)).thenReturn(sprint);
        doNothing().when(sprintRepository).flush();

        mvc.perform(
                put("/project/{project_id}/sprint/update/{sprint_id}", projectId, sprintId)
                .flashAttr("sprint", sprint)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            )
            .andExpect(status().isOk());

        verify(sprintRepository, times(1)).findOne(sprintId);
        verify(sprintRepository, times(1)).flush();
        verifyNoMoreInteractions(sprintRepository);
    }

    @Test
    public void cannotUpdateSprintWhenFieldIsEmptyThrowException() throws Exception
    {
        Sprint sprint = new Sprint();
        sprint.setName("test name");
        sprint.setGoal("test goal");

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                put("/project/{project_id}/sprint/update/{sprint_id}", projectId, sprintId)
                .flashAttr("sprint", sprint)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            )
            .andExpect(status().is(422))
            .andExpect(jsonPath("$.code", is("InvalidRequest")))
            .andExpect(jsonPath("$.message", is("Invalid sprint")))
            .andExpect(jsonPath("$.fieldErrorResources[0].resource", is("sprint")))
            .andExpect(jsonPath("$.fieldErrorResources[0].field", is("status")))
            .andExpect(jsonPath("$.fieldErrorResources[0].message", is("may not be null")));
    }

    @Test
    public void shouldDeleteSprintById() throws Exception
    {
        doNothing().when(sprintRepository).delete(sprintId);
        doNothing().when(sprintRepository).flush();

        mvc.perform(
                delete("/project/{project_id}/sprint/delete/{sprint_id}", projectId, sprintId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            )
            .andExpect(status().isOk());

        verify(sprintRepository, times(1)).delete(sprintId);
        verify(sprintRepository, times(1)).flush();
        verifyNoMoreInteractions(sprintRepository);
    }
}
