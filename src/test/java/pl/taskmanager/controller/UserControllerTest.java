package pl.taskmanager.controller;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import pl.taskmanager.entity.User;
import pl.taskmanager.entity.UserRole;
import pl.taskmanager.entity.UserRoleType;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.repository.UserRoleRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
@WithMockUser(username = "test")
public class UserControllerTest {

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserRoleRepository userRoleRepository;

    @MockBean
    private BindingResult bindingResult;

    @Autowired
    MockMvc mvc;

    private static final int userId = 1;

    @Before
    public void setUp()
    {
        initMocks(this);
    }

    @Test
    public void shouldPrepareDataToViewAllUsers() throws Exception
    {
        int page = 1;
        int size = 10;
        int totalElements = 10;

        PageRequest pageRequest = new PageRequest(page-1, size);
        Page<User> pagebuilder = new PageImpl<User>(
                new ArrayList<User>(), pageRequest, totalElements
        );

        when(userRepository.findAll(pageRequest)).thenReturn(pagebuilder);

        mvc.perform(
                get("/secured/user/")
                .param("page", ""+page)
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))
            .andExpect(model().attribute("view", "user/user_list"))
            .andExpect(model().attribute("currentPage", page))
            .andExpect(model().attribute("users", pagebuilder));

        verify(userRepository, times(1)).findAll(pageRequest);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void shouldPrepareDataToCreateUser() throws Exception
    {
        List<UserRole> userRoleList = new ArrayList<>();

        when(userRoleRepository.findAll()).thenReturn(userRoleList);

        mvc.perform(
                get("/secured/user/add")
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))

            .andExpect(model().attribute("user", hasProperty("id", is(0))))
            .andExpect(model().attribute("user", hasProperty("username", isEmptyOrNullString())))
            .andExpect(model().attribute("user", hasProperty("email", isEmptyOrNullString())))
            .andExpect(model().attribute("user", hasProperty("password", isEmptyOrNullString())))
            .andExpect(model().attribute("user", hasProperty("userRoles", emptyCollectionOf(UserRole.class))))
            .andExpect(model().attribute("allUserRoles", userRoleList))
            .andExpect(model().attribute("view","user/user_add"));

        verify(userRoleRepository, times(1)).findAll();
        verifyNoMoreInteractions(userRoleRepository);
    }

    @Test
    public void shouldCreateUser() throws Exception
    {
        Set<UserRole> userRoles = new HashSet<UserRole>();
        UserRole userRole = new UserRole();
        userRole.setName(UserRoleType.ROLE_DEVELOPER);
        userRoles.add(userRole);

        User user = new User(userId);
        user.setUsername("test username");
        user.setPassword("password12");
        user.setEmail("test.test@test.pl");
        user.setUserRoles(userRoles);

        when(userRepository.save(user)).thenReturn(user);

        mvc.perform(
                post("/secured/user/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("user", user)
            )
            .andExpect(redirectedUrl("/secured/user/"))
            .andExpect(flash().attribute("statement","Success create User"));

        verify(userRepository, times(1)).save(user);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void cannotCreateUserWhenFieldIsEmptyThrowException() throws Exception
    {
        User user = new User(userId);

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                post("/secured/user/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("user", user)
            )
            .andExpect(status().is(422))
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasErrors("user"));
    }

    @Test
    public void shouldPrepareDataToUpdateUser() throws Exception
    {
        User user = new User(userId);

        when(userRepository.findOne(userId)).thenReturn(user);

        List<UserRole> userRoleList = new ArrayList<>();
        when(userRoleRepository.findAll()).thenReturn(userRoleList);

        mvc.perform(
                get("/secured/user/update/{id}", userId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))
            .andExpect(model().attribute("user", user))
            .andExpect(model().attribute("allUserRoles", userRoleList))
            .andExpect(model().attribute("view","user/user_edit"));

        verify(userRepository, times(1)).findOne(user.getId());
        verify(userRoleRepository, times(1)).findAll();
        verifyNoMoreInteractions(userRepository, userRoleRepository);
    }

    @Test
    public void shouldUpdateUser() throws Exception
    {
        Set<UserRole> userRoles = new HashSet<UserRole>();
        UserRole userRole = new UserRole();
        userRole.setName(UserRoleType.ROLE_DEVELOPER);
        userRoles.add(userRole);

        User user = new User(userId);
        user.setUsername("test username");
        user.setPassword("password12");
        user.setEmail("test.test@test.pl");
        user.setUserRoles(userRoles);

        when(userRepository.findOne(userId)).thenReturn(user);
        doNothing().when(userRepository).flush();

        mvc.perform(
                put("/secured/user/update/{id}", userId)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .flashAttr("user", user)
            )
            .andExpect(redirectedUrl("/secured/user/"))
            .andExpect(flash().attribute("statement","Success update User"));

        verify(userRepository, times(1)).findOne(user.getId());
        verify(userRepository, times(1)).flush();
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void cannotUpdateUserWhenFieldIsEmptyThrowException() throws Exception
    {
        List<UserRole> userRoles = new ArrayList<UserRole>();

        User user = new User(userId);

        when(bindingResult.hasErrors()).thenReturn(true);
        when(userRoleRepository.findAll()).thenReturn(userRoles);

        mvc.perform(
                put("/secured/user/update/{id}", userId)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .flashAttr("user", user)
            )
            .andExpect(status().is(422))
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasErrors("user"))
            .andExpect(view().name("base"))
            .andExpect(model().attribute("view", "user/user_edit"))
            .andExpect(model().attribute("id", userId))
            .andExpect(model().attribute("user", user))
            .andExpect(model().attribute("allUserRoles", userRoles));

        verify(userRoleRepository, times(1)).findAll();
        verifyNoMoreInteractions(userRoleRepository);
    }

    @Test
    public void shouldEnableUser() throws Exception
    {
        User user = new User(userId);
        user.setEnable(false);

        when(userRepository.findOne(userId)).thenReturn(user);
        doNothing().when(userRepository).flush();

        mvc.perform(
                get("/secured/user/update/status/{id}", userId)
            )
            .andExpect(redirectedUrl("/secured/user/"))
            .andExpect(flash().attribute("statement","Success update User status"));

        assertEquals(true, user.isEnable());

        verify(userRepository, times(1)).findOne(user.getId());
        verify(userRepository, times(1)).flush();
        verifyNoMoreInteractions(userRepository);
    }
}
